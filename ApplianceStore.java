import java.util.Scanner;
public class ApplianceStore
{
	public static void main(String[] args)
	{
		Scanner scan = new Scanner(System.in);
		Toaster[] toasters = new Toaster[4];
		for(int i=0, num=1; i<toasters.length; i++, num++)
		{
			//toasters[i] = new Toaster();
			System.out.println("Enter the brand of toaster number " + num + " .");
			//toasters[i].setBrand(scan.nextLine());
			String brand = scan.next();
			System.out.println("Enter the color of toaster number " +num + " .");
			//toasters[i].setColor(scan.nextLine());
			String color = scan.next();
			System.out.println("Enter the number of slots that toaster number " + num + " has.");
			//toasters[i].setNumOfSlots(Integer.parseInt(scan.nextLine()));
			int numOfSlots = scan.nextInt();
			toasters[i] = new Toaster(brand, color,numOfSlots);
		}
		System.out.println("The brand of the last toaster is " + toasters[3].getBrand() + " . The color of the last toaster is " + toasters[3].getColor() + " . The number of slots the last toaster has is " + toasters[3].getNumOfSlots() + " .");
		System.out.println("Modify the last toasters brand.");
		toasters[3].setBrand(scan.next());
		System.out.println("Modify the last toasters color.");
		toasters[3].setColor(scan.next());
		System.out.println("Modify the last toasters number of slots.");
		toasters[3].setNumOfSlots(scan.nextInt());
		System.out.println("AFTER MODIFICATION: The brand of the last toaster is " + toasters[3].getBrand() + " . The color of the last toaster is " + toasters[3].getColor() + " . The number of slots the last toaster has is " + toasters[3].getNumOfSlots() + " .");
		//toasters[2].printBrand();
		//System.out.println("How much bread do you want to put into the last toaster?");
		//int bread = scan.nextInt();
		//toasters[3].toastBread(bread);
		System.out.println("Please enter what sound you would like the toaster to make when the timer is over.");
		toasters[1].setTimer();
	}
	
}