import java.util.Scanner;
public class Toaster
{
	private String brand;
	private String color;
	private int numOfSlots;
	private String alarmSound;

	
	public void printBrand()
	{
		System.out.println("This method prints the brand of the toaster which is: " + this.brand);
	}
	
	public void toastBread(int numOfBread)
	{
		if(numOfBread > numOfSlots)
		{
			System.out.println("That's way too much bread");
		}
		else
		{
			System.out.println("Bread has been inserted and is currently getting toasted.");
		}
	}
	
	public void setTimer()
	{
		Scanner scan = new Scanner(System.in);
		setAlarmSound(scan.nextLine());
		System.out.println("Insert how long you want the timer to be (in seconds)");
		int numOfSeconds = scan.nextInt();
		
		if (numOfSeconds > 0)
		{
			for(int i=0; i<numOfSeconds; i++)
			{
				System.out.println(i);
			}
		}
		else
		{
			System.out.println("The number you entered is either negative or 0 please enter a valid number of seconds next time.");
		}
		System.out.println(this.alarmSound);
	}
	
	private boolean isValid(String alarmSound)
	{
		boolean isValid = (alarmSound.equals("BeeepBeeepBeeep") || alarmSound.equals("NeeeNeeeNeee") || alarmSound.equals("BooopBooopBooop"));
		return isValid;
	}
	
	public void setBrand(String newBrand)
	{
		this.brand = newBrand;
	}
	
	public String getBrand()
	{
		return this.brand;
	}
	
	public void setColor(String newColor)
	{
		this.color = newColor;
	}
	
	public String getColor()
	{
		return this.color;
	}
	
	public void setNumOfSlots(int newNumOfSlots)
	{
		this.numOfSlots = newNumOfSlots;
	}
	
	public int getNumOfSlots()
	{
		return this.numOfSlots;
	}
	
	public void setAlarmSound(String newAlarmSound)
	{
		if (isValid(newAlarmSound))
		{
			this.alarmSound = newAlarmSound;
		}
		else 
		{
			System.out.println("The alarm sound you chose is invalid please choose one of the valid ones for now the alarm will remain the default sound.");
			System.out.println("Here are the options BeeepBeeepBeeep or NeeeNeeeNeee or BooopBooopBooop.");
		}
	}
	
	public String getAlarmSound()
	{
		return this.alarmSound;
	}
	
	public Toaster(String brand, String color, int numOfSlots)
	{
		this.brand = brand;
		this.color = color;
		this.numOfSlots = numOfSlots;
		this.alarmSound = "BeeepBeeepBeeep";
	}
}